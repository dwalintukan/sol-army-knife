# SolConstants

Solidity testing convenience constants.

## Install

```bash
npm install --save-dev sol-army-knife
```

## Usage

```js
const { getConstants } = require('sol-army-knife')
contract('ABEvent', (accounts) => {
  const { ACCT0, ACCT1, INVALID_ADDR, MAX_GAS } = getConstants(accounts)

  // your tests...
})
```

## Available Constants

```js
{
    ACCT0: accounts[0],
    ACCT1: accounts[1],
    ACCT2: accounts[2],
    ACCT3: accounts[3],
    ACCT4: accounts[4],
    ACCT5: accounts[5],
    ACCT6: accounts[6],
    ACCT7: accounts[7],
    ACCT8: accounts[8],
    ACCT9: accounts[9],
    INVALID_ADDR: '0x0000000000000000000000000000000000000000',
    MAX_GAS: 4712388,
}
```
