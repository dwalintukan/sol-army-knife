const { find, each, isPlainObject, isArray, isString, isNumber } = require('lodash')

const web3 = global.web3
const { toBN, padRight } = web3.utils

module.exports = class SolUtils {
  /**
   * Adds a hex prefix (0x) to the string.
   * @param {string} str String to add the hex prefix to
   * @return {string} String with hex prefix
   */
  static addHexPrefix(str) {
    if (!isString(str)) return str
    if (!str.startsWith('0x')) return `0x${str}`
    return str
  }

  /**
   * Removes a hex prefix (0x) from the string, if it exists.
   * @param {string} str String to remove the hex prefix from
   * @return {string} String without hex prefix
   */
  static removeHexPrefix(str) {
    if (!isString(str)) return str
    if (str.startsWith('0x')) return str.substr(2)
    return str
  }

  /**
   * Converts the amount to a lower denomination.
   * @param {number|string} amount Amount to convert
   * @param {number} decimals Number of decimals
   * @return {BN} Amount as BN.js instance
   */
  static toDenomination(amount, decimals = 0) {
    if (((isString(amount) && amount.includes('.'))
      || (isNumber(amount) && amount % 1 != 0))
      && decimals > 0) {
      const arr = amount.toString().split('.')
      const whole = arr[0]
      const dec = arr[1]

      const wholeBn = toBN(10 ** decimals).mul(toBN(whole))
      const decBn = toBN(padRight(dec, decimals))
      return wholeBn.add(decBn)
    }

    return toBN(10 ** decimals).mul(toBN(amount))
  }

  /**
   * Gets the object from the ABI given the name and type
   * @param {object} abi ABI to search in
   * @param {string} name Name of the function or event
   * @param {string} type One of: [function, event]
   * @return {object|undefined} Object found in ABI
   */
  static getAbiObject(abi, name, type) {
    if (!abi) return undefined;
    return find(abi, { name, type });
  }

  /**
   * Gets an event signature from the ABI given the name
   * @param {object} abi ABI to search in
   * @param {string} name Name of the function or event
   * @return {object|undefined} Object found in ABI
   */
  static getEventSig(abi, name) {
    const obj = Utils.getAbiObject(abi, name, 'event')
    return web3.eth.abi.encodeEventSignature(obj)
  }
}
