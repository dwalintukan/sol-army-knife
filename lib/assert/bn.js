const { assert } = require('chai')

const web3 = global.web3

module.exports = {
  /**
   * Asserts that a == b.
   * @param a Can be any BN.js compatible types.
   * @param b Can be any BN.js compatible types.
   */
  bnEqual: (a, b) => {
    let convertedA = a
    if (!web3.utils.isBN(a)) convertedA = web3.utils.toBN(a)
    
    let convertedB = b
    if (!web3.utils.isBN(b)) convertedB = web3.utils.toBN(b)
  
    assert.equal(convertedA.toString(10), convertedB.toString(10))
  },

  /**
   * Asserts that a > b.
   * @param a Can be any BN.js compatible types.
   * @param b Can be any BN.js compatible types.
   */
  bnGT: (a, b) => {
    let convertedA = a
    if (!web3.utils.isBN(a)) convertedA = web3.utils.toBN(a)
    
    let convertedB = b
    if (!web3.utils.isBN(b)) convertedB = web3.utils.toBN(b)
  
    assert.isTrue(convertedA.gt(convertedB))
  },

  /**
   * Asserts that a >= b.
   * @param a Can be any BN.js compatible types.
   * @param b Can be any BN.js compatible types.
   */
  bnGTE: (a, b) => {
    let convertedA = a
    if (!web3.utils.isBN(a)) convertedA = web3.utils.toBN(a)
    
    let convertedB = b
    if (!web3.utils.isBN(b)) convertedB = web3.utils.toBN(b)
  
    assert.isTrue(convertedA.gte(convertedB))
  },

  /**
   * Asserts that a < b.
   * @param a Can be any BN.js compatible types.
   * @param b Can be any BN.js compatible types.
   */
  bnLT: (a, b) => {
    let convertedA = a
    if (!web3.utils.isBN(a)) convertedA = web3.utils.toBN(a)
    
    let convertedB = b
    if (!web3.utils.isBN(b)) convertedB = web3.utils.toBN(b)
  
    assert.isTrue(convertedA.lt(convertedB))
  },

  /**
   * Asserts that a <= b.
   * @param a Can be any BN.js compatible types.
   * @param b Can be any BN.js compatible types.
   */
  bnLTE: (a, b) => {
    let convertedA = a
    if (!web3.utils.isBN(a)) convertedA = web3.utils.toBN(a)
    
    let convertedB = b
    if (!web3.utils.isBN(b)) convertedB = web3.utils.toBN(b)
  
    assert.isTrue(convertedA.lte(convertedB))
  },
}
