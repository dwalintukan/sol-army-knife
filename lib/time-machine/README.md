# TimeMachine

Block and time manager for Solidity testing.

## Install

```bash
npm install --save-dev sol-army-knife
```

## Usage

```js
const { TimeMachine } = require('sol-army-knife')
const timeMachine = new TimeMachine(global.web3)
```

### snapshot/revert

```js
// test.js
contract('ExampleContract', (accounts) => {
  beforeEach(async () => {
    await timeMachine.snapshot()
  })
  
  afterEach(async () => {
    await timeMachine.revert()
  })

  // Your tests here...
})
```

### mine

```js
// advance 10 blocks
await timeMachine.mine(10)
```

### mineTo

```js
// advance to block number 10
await timeMachine.mineTo(10)
```

### increaseTime

```js
// advance 1 block and set blockTime to current blockTime plus 10 seconds
await timeMachine.increaseTime(10)
```
