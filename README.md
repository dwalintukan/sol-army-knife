# sol-army-knife

Swiss Army Knife for Solidity testing. Convenient tools you will need when writing Javascript tests for your Solidity contracts.

## Requirements

1. Node/NPM
2. Solidity contract to test
3. Truffle (this library uses Truffle's injected global.web3)

## Install

```bash
npm install --save-dev sol-army-knife
```

## SolAssert

Helpful assertions for Solidity Javascript testing. See [README](/lib/assert/README.md).

## SolConstants

Solidity testing convenience constants. See [README](/lib/constants/README.md).

## SolTimeMachine

Block and time manager for Solidity testing. See [README](/lib/time-machine/README.md).
